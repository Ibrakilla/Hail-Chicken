/** Category Service */
import { Injectable } from '@angular/core';
@Injectable()
export class CategoryService {
  constructor() {}
  /** All Categories */
  getAllCategories() {
    return [{
      id: 1,
      name: 'التأمين الطبي',
      image: 'assets/img/medical.png'
    }, {
      id: 2,
      name: 'التأمين البحري',
      image: 'assets/img/Marine.png'
    }, {
      id: 3,
      name: 'تأمين الحريق',
      image: 'assets/img/fire.png'
    }, {
      id: 4,
      name: 'تأمين السيارات',
      image: 'assets/img/car.png'
    }, {
      id: 5,
      name: 'التأمين التكافلي',
      image: 'assets/img/life.png'
    }, {
      id: 6,
      name: 'التأمين الهندسي',
      image: 'assets/img/engineering.png'
    }, {
      id: 7,
      name: 'تأمين الحوادث المتنوعة',
      image: 'assets/img/accident.png'
    }, {
      id: 8,
      name: 'تأمين السفر',
      image: 'assets/img/travel.png'
    },
    {
      id: 9,
      name: 'التأمين الزراعي',
      image: 'assets/img/farm.png'
    },
    {
      id: 10,
      name: 'تأمين الثروة الحيوانية',
      image: 'assets/img/animals.png'
    }, ]
  }
}
