/** Represents a Component of contactus page. */

/** Imports Modules */
import { Component, OnInit } from "@angular/core";
import { IonicPage } from "ionic-angular";
import { RestProvider } from "../../providers/rest/rest";
import {
  ToastController,
  NavController,
  LoadingController
} from "ionic-angular";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from "@angular/forms";
import { StorageServiceProvider } from "../../providers/storage-service/storage-service";
@IonicPage()
@Component({
  selector: "customer-login",
  templateUrl: "customer-login.html"
})
export class CustomerloginComponent implements OnInit {
  myForm: FormGroup;
  usersPolicyInfo: any;
  userInfo = { policyNo: "", plateNo: "" };

  constructor(
    public formBuilder: FormBuilder,
    public restProvider: RestProvider,
    public navCtrl: NavController,
    private toastCtrl: ToastController,
    private StorageServiceProvider: StorageServiceProvider,
    private readonly loadingCtrl: LoadingController
  ) {
    this.navCtrl = navCtrl;
  }

  gotoTargetComponent(value) {
    if (value === "Category") {
      // When value is Profile
      this.navCtrl.push("CategoryComponent");
    } else if (value === "AboutUs") {
      // When value is About Us
      this.navCtrl.push("AboutUsComponent");
    } else if (value === "claims") {
      // When value is About Us
      this.navCtrl.push("ClaimsComponent");
    } else if (value === "Location") {
      // When value is About Us
      this.navCtrl.push("LocationComponent");
    } else if (value === "ContactUs") {
      // When value is About Us
      this.navCtrl.push("ContactUsComponent");
    } else if (value === "new-account") {
      // When value is About Us
      this.navCtrl.push("NewAccountComponent");
    } else if (value === "Customer-Portal") {
      // When value is About Us
      this.navCtrl.setRoot("PortalTabsPage");
    }
  }

  presentSuccessToast() {
    let toast = this.toastCtrl.create({
      message: "تم ارسال الرسالة بنجاح",
      duration: 3000,
      position: "middle"
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }
  presentErrorToast() {
    let toast = this.toastCtrl.create({
      message: "بيانات الدخول غير صحيحة، الرجاء المحاولة مرة أخرى",
      duration: 3000,
      position: "middle"
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }
  ngOnInit(): any {
    this.myForm = this.formBuilder.group({
      policyNo: [
        "",
        [
          Validators.required,
          Validators.minLength(1),
          this.policyNoValidator.bind(this)
        ]
      ],
      plateNo: [
        "",
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(300),
          this.plateNoValidator.bind(this)
        ]
      ]
    });
  }

  onSubmit() {}

  isValid(field: string) {
    let formField = this.myForm.get(field);
    return formField.valid || formField.pristine;
  }

  policyNoValidator(control: FormControl): { [s: string]: boolean } {
    if (!control.value.match("^[a-zA-Z ,\u0600-\u06FF,.'-]+$")) {
      return {};
    }
  }
  plateNoValidator(control: FormControl): { [s: string]: boolean } {
    if (!control.value.match("^[a-zA-Z ,\u0600-\u06FF,.'-,\\d]+$")) {
      return {};
    }
  }
}
