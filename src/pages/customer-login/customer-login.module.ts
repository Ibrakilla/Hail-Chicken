import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { CustomerloginComponent } from './customer-login';

@NgModule({
  declarations: [
    CustomerloginComponent,
  ],
  imports: [
    IonicPageModule.forChild(CustomerloginComponent),
    TranslateModule
  ],
  exports: [
    CustomerloginComponent,
    TranslateModule
  ]
})
export class customerLoginModule {}
