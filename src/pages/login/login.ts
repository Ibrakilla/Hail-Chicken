/** Represents a Component of contactus page. */

/** Imports Modules */
import { Component, OnInit } from "@angular/core";
import { IonicPage } from "ionic-angular";
import { RestProvider } from "../../providers/rest/rest";
import {
  ToastController,
  NavController,
  LoadingController
} from "ionic-angular";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from "@angular/forms";
import { StorageServiceProvider } from "../../providers/storage-service/storage-service";
@IonicPage()
@Component({
  selector: "login",
  templateUrl: "login.html"
})
export class LoginComponent implements OnInit {
  myForm: FormGroup;
  usersPolicyInfo: any;
  userInfo = { policyNo: "", plateNo: "" };

  constructor(
    public formBuilder: FormBuilder,
    public restProvider: RestProvider,
    public navCtrl: NavController,
    private toastCtrl: ToastController,
    private StorageServiceProvider: StorageServiceProvider,
    private readonly loadingCtrl: LoadingController
  ) {
    this.navCtrl = navCtrl;
  }
  presentSuccessToast() {
    let toast = this.toastCtrl.create({
      message: "تم ارسال الرسالة بنجاح",
      duration: 3000,
      position: "middle"
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }
  presentErrorToast() {
    let toast = this.toastCtrl.create({
      message: "بيانات الدخول غير صحيحة، الرجاء المحاولة مرة أخرى",
      duration: 3000,
      position: "middle"
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }
  ngOnInit(): any {
    this.myForm = this.formBuilder.group({
      policyNo: [
        "",
        [
          Validators.required,
          Validators.minLength(1),
          this.policyNoValidator.bind(this)
        ]
      ],
      plateNo: [
        "",
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(300),
          this.plateNoValidator.bind(this)
        ]
      ]
    });
  }

  onSubmit() {
    let loader = this.loadingCtrl.create({
    });
    loader.present();
    this.restProvider.login(this.userInfo.policyNo, this.userInfo.plateNo).then(
      result => {
        console.log(result);
        if (Object.keys(result).length == 0) {
          this.presentErrorToast();
          loader.dismiss();
        } else {
          this.usersPolicyInfo = result;

          console.log(this.usersPolicyInfo[0].ClientName);
          //SNo,PolicyNo,PolicyHolder,ClientName,PlateNo
          this.StorageServiceProvider.PolicyInfo(
            this.usersPolicyInfo[0].SNo,
            this.usersPolicyInfo[0].PolicyNo,
            this.usersPolicyInfo[0].PolicyHolder,
            this.usersPolicyInfo[0].ClientName,
            this.userInfo.plateNo,
            this.usersPolicyInfo[0].PeriodFrom,
            this.usersPolicyInfo[0].PeriodTo
          );
          loader.dismiss();
          this.navCtrl.setRoot("PortalTabsPage");
        }
      },
      err => {
        this.presentErrorToast();
        loader.dismiss();
        console.log(err);
      }
    );
  }

  isValid(field: string) {
    let formField = this.myForm.get(field);
    return formField.valid || formField.pristine;
  }

  policyNoValidator(control: FormControl): { [s: string]: boolean } {
    if (!control.value.match("^[a-zA-Z ,\u0600-\u06FF,.'-]+$")) {
      return {};
    }
  }
  plateNoValidator(control: FormControl): { [s: string]: boolean } {
    if (!control.value.match("^[a-zA-Z ,\u0600-\u06FF,.'-,\\d]+$")) {
      return {};
    }
  }
}
