/** Represents a Component of AboutUs. */

/** Imports Modules */
import { Component } from "@angular/core";
import { NavController, IonicPage, ModalController } from "ionic-angular";

@IonicPage()
@Component({
  selector: "aboutus",
  templateUrl: "aboutus.html"
})
export class AboutUsComponent {
  segment: any;
  constructor(
    public modalCtrl: ModalController,
    public navCtrl: NavController,
  ) {
    this.segment = "design";
    this.navCtrl = navCtrl;

  }
  goBack() {
    let modal = this.modalCtrl.create("HomeComponent");
    modal.present();
  }
}
