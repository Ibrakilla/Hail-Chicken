import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController
} from "ionic-angular";
import { App,MenuController } from "ionic-angular";
import { Storage } from "@ionic/storage";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { RestProvider } from "../../providers/rest/rest";
import { Component, OnInit } from "@angular/core";
import { ToastController } from "ionic-angular";
import { LoaderService } from '../../common/services/loader.service';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from "@angular/forms";
/**
 * Generated class for the NewClaimPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-user-claim",
  templateUrl: "user-claim.html"
})
export class UserClaimPage {
  claims: any;
  pendingclaims: any;
  segment: any;
  apiPendingClaims = "http://elnileinapi.com/api/policies/getclaims";
  apiUserClaims = "http://elnileinapi.com/api/policies/GetPendingClaims";
  myForm: FormGroup;
  userInfo = { name: "", email: "", phone: "", comment: "" };

  ApiMsg: { To: string; Cc: string; Subject: string } = {
    To: "ibrahim_elhussein@hotmail.com",
    Cc: "ibarhim@oasisoft.net",
    Subject: "Alnilein Mobile App - Complaints Form"
  };
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restProvider: RestProvider,
    private menu: MenuController,
    public http: HttpClient,
    public appCtrl: App,
    private readonly loadingCtrl: LoadingController,
    private storage: Storage,
    public formBuilder: FormBuilder,
    private toastCtrl: ToastController,
    private loaderService: LoaderService
  ) {
  }
  ionViewDidEnter() {
    this.menu.swipeEnable(false);

    // If you have more than one side menu, use the id like below
    // this.menu.swipeEnable(false, 'menu1');
  }

  ionViewDidLeave() {
    // Don't forget to return the swipe to normal, otherwise
    // the rest of the pages won't be able to swipe to open menu
    this.menu.swipeEnable(true);

    // If you have more than one side menu, use the id like below
    // this.menu.swipeEnable(true, 'menu1');
   }
  ionViewDidLoad() {
    console.log("ionViewDidLoad PolicyPage");
  }
  Logout() {
    this.appCtrl.getRootNavs()[0].push("WelcomeComponent");
  }
  presentSuccessToast() {
    let toast = this.toastCtrl.create({
      message: "تم ارسال الرسالة بنجاح",
      duration: 3000,
      position: "middle"
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }
  presentErrorToast() {
    let toast = this.toastCtrl.create({
      message: "خطأ في الإرسال, الرجاء اعادة المحاولة",
      duration: 3000,
      position: "middle"
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }
  ngOnInit(): any {
    this.myForm = this.formBuilder.group({
      name: [
        "",
        [
          Validators.required,
          Validators.minLength(1),
          this.nameValidator.bind(this)
        ]
      ],
      comment: [
        "",
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(300),
          this.commentValidator.bind(this)
        ]
      ],
      phone: ["", this.phoneValidator.bind(this)],
      email: ["", [Validators.required, this.emailValidator.bind(this)]]
    });
  }

  onSubmit() {
    this.loaderService.presentLoading();
    this.restProvider.addUser2(this.ApiMsg, this.userInfo).then(
      result => {
        this.loaderService.hideLoading();
        console.log(result);
        this.presentSuccessToast();
        this.userInfo = { name: "", email: "", phone: "", comment: "" };
        this.ngOnInit();
      },
      err => {
        this.loaderService.hideLoading();
        this.presentErrorToast();
        console.log(err);
      }
    );
  }

  isValid(field: string) {
    let formField = this.myForm.get(field);
    return formField.valid || formField.pristine;
  }

  nameValidator(control: FormControl): { [s: string]: boolean } {
    if (!control.value.match("^[a-zA-Z ,\u0600-\u06FF,.'-]+$")) {
      return { invalidName: true };
    }
  }
  commentValidator(control: FormControl): { [s: string]: boolean } {
    if (!control.value.match("^[a-zA-Z ,\u0600-\u06FF,.'-,\\d]+$")) {
      return { };
    }
  }
  phoneValidator(control: FormControl): { [s: string]: boolean } {
    if (control.value !== "") {
      if (!control.value.match("^(?:[9\u0660-\u0669\u06F0-\u06F9]|(?:[0-9])){0,15}$")) {
        return { invalidPhone: true };
      }
    }
  }

  emailValidator(control: FormControl): { [s: string]: boolean } {
    if (
      !control.value
        .toLowerCase()
        .match(
          "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"
        )
    ) {
      return { invalidEmail: true };
    }
  }
}
