import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { DriverLoginComponent } from './driver-login';

@NgModule({
  declarations: [
    DriverLoginComponent,
  ],
  imports: [
    IonicPageModule.forChild(DriverLoginComponent),
    TranslateModule
  ],
  exports: [
    DriverLoginComponent,
    TranslateModule
  ]
})
export class driverLoginModule {}
