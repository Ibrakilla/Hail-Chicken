import { Component } from "@angular/core";
import { IonicPage, NavController } from "ionic-angular";
import { App,MenuController } from "ionic-angular";
import { Storage } from "@ionic/storage";
import { Platform, Config, Nav } from "ionic-angular";
/**
 * Generated class for the PortalTabsPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-portal-tabs",
  templateUrl: "portal-tabs.html"
})
export class PortalTabsPage {
  policyRoot = "PolicyPage";
  newClaimRoot = "NewClaimPage";
  userClaimRoot = "UserClaimPage";
  constructor(
    public navCtrl: NavController,
    private menu: MenuController,
    public appCtrl: App,
    private storage: Storage,
    public platform: Platform
  ) {}
  ionViewDidEnter() {
    this.menu.swipeEnable(false);

    // If you have more than one side menu, use the id like below
    // this.menu.swipeEnable(false, 'menu1');
  }

  ionViewDidLeave() {
    // Don't forget to return the swipe to normal, otherwise
    // the rest of the pages won't be able to swipe to open menu
    this.menu.swipeEnable(true);

    // If you have more than one side menu, use the id like below
    // this.menu.swipeEnable(true, 'menu1');
   }
  CloseApp() {
    this.platform.exitApp();
  }
}
