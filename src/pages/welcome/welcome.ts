/** Represents a Component of Components page. */

/** Import Modules */
import { Component } from "@angular/core";
import { NavController, IonicPage, ModalController } from "ionic-angular";
import { Storage } from "@ionic/storage";
@IonicPage()
@Component({
  selector: "welcome",
  templateUrl: "welcome.html"
})
export class WelcomeComponent {
  logged: string;
  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    private storage: Storage
  ) {
    this.navCtrl = navCtrl;
    storage.set("name", "Max");
  }

  /**
   * gotoTargetComponent(value)
   * This function works on click event
   * when user click any component its get the value of this specific event and
   * NavController set this component and will take user to that component
   */

  gotoTargetComponent(value) {
    if (value === "CustomerLogin") {
      // When value is About Us
      this.navCtrl.push("CustomerloginComponent");
    }
    else if (value === "CustomerPortal") {
      // When value is About Us
      this.navCtrl.setRoot("PortalTabsPage");
    }
    else if (value === "DriverLogin") {
      // When value is About Us
      this.navCtrl.push("DriverLoginComponent");
    }
  }
}
