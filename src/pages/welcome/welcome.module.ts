import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { TranslateModule } from "ng2-translate/ng2-translate";
import { WelcomeComponent } from "./welcome";

@NgModule({
  declarations: [WelcomeComponent],
  imports: [IonicPageModule.forChild(WelcomeComponent), TranslateModule],
  exports: [WelcomeComponent, TranslateModule]
})
export class HomeModule {}
