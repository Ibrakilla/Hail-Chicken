import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { App,MenuController  } from "ionic-angular";
import { Storage } from "@ionic/storage";
/**
 * Generated class for the PolicyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-policy",
  templateUrl: "policy.html"
})
export class PolicyPage {
  ClientName: string;
  PolicyNo: string;
  PeriodFrom: string;
  PeriodTo: string;
  PlateNo: string;
  PolicyHolder: string;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public appCtrl: App,
    private menu: MenuController,
    private storage: Storage
  ) {
    this.storage.get("ClientName").then(ClientName => {
      console.log(ClientName);
      this.ClientName = ClientName;
    });
    this.storage.get("PolicyNo").then(PolicyNo => {
      console.log(PolicyNo);
      this.PolicyNo = PolicyNo;
    });
    this.storage.get("PeriodFrom").then(PeriodFrom => {
      console.log(PeriodFrom);
      this.PeriodFrom = PeriodFrom;
    });
    this.storage.get("PeriodTo").then(PeriodTo => {
      console.log(PeriodTo);
      this.PeriodTo = PeriodTo;
    });
    this.storage.get("PlateNo").then(PlateNo => {
      console.log(PlateNo);
      this.PlateNo = PlateNo;
    });
    this.storage.get("PolicyHolder").then(PolicyHolder => {
      console.log(PolicyHolder);
      this.PolicyHolder = PolicyHolder;
    });
  }
  ionViewDidEnter() {
    this.menu.swipeEnable(false);

    // If you have more than one side menu, use the id like below
    // this.menu.swipeEnable(false, 'menu1');
  }

  ionViewDidLeave() {
    // Don't forget to return the swipe to normal, otherwise
    // the rest of the pages won't be able to swipe to open menu
    this.menu.swipeEnable(true);

    // If you have more than one side menu, use the id like below
    // this.menu.swipeEnable(true, 'menu1');
   }
  ionViewDidLoad() {
    console.log("ionViewDidLoad PolicyPage");
  }
  Logout() {
    this.storage.clear();
    this.appCtrl.getRootNavs()[0].push("WelcomeComponent");
  }
  GoBack() {
    this.storage.set("LoggedIn", true);
    this.appCtrl.getRootNavs()[0].push("HomeComponent");
  }
}
