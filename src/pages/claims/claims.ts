/** Represents a Component of AboutUs. */

/** Imports Modules */
import { Component, ViewChild, OnInit, Renderer } from "@angular/core";
import { NavController, IonicPage, ModalController } from "ionic-angular";

@IonicPage()
@Component({
  selector: "claims",
  templateUrl: "claims.html"
})
export class ClaimsComponent implements OnInit {
  segment: any;

  AccordionExpanded = false;
  AccordionExpanded2 = false;
  AccordionExpanded3 = false;
  AccordionExpanded4 = false;
  @ViewChild("c1") cardContent: any;
  icon: string = "md-add";
  @ViewChild("c2") cardContent2: any;
  icon2: string = "md-add";
  @ViewChild("c3") cardContent3: any;
  icon3: string = "md-add";
  @ViewChild("c4") cardContent4: any;
  icon4: string = "md-add";

  constructor(
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public renderer: Renderer
  ) {
    this.segment = "design";
    this.navCtrl = navCtrl;
  }
  goBack() {
    let modal = this.modalCtrl.create("HomeComponent");
    modal.present();
  }

  ngOnInit() {
    console.log(this.cardContent);
    this.renderer.setElementStyle(
      this.cardContent.nativeElement,
      "webkitTransition",
      "max-height 500ms, padding 500ms"
    );
    this.renderer.setElementStyle(
      this.cardContent2.nativeElement,
      "webkitTransition",
      "max-height 500ms, padding 500ms"
    );
    this.renderer.setElementStyle(
      this.cardContent3.nativeElement,
      "webkitTransition",
      "max-height 500ms, padding 500ms"
    );
    this.renderer.setElementStyle(
      this.cardContent4.nativeElement,
      "webkitTransition",
      "max-height 500ms, padding 500ms"
    );
  }
  ToggleAccordion() {
    if (this.AccordionExpanded) {
      this.renderer.setElementStyle(
        this.cardContent.nativeElement,
        "max-height",
        "0px"
      );
      this.renderer.setElementStyle(
        this.cardContent.nativeElement,
        "padding",
        "0px 16px"
      );
    } else {
      this.renderer.setElementStyle(
        this.cardContent.nativeElement,
        "max-height",
        "500px"
      );
      this.renderer.setElementStyle(
        this.cardContent.nativeElement,
        "padding",
        "13px 16px"
      );
    }
    this.AccordionExpanded = !this.AccordionExpanded;
    this.icon = this.icon == "md-add" ? "md-remove" : "md-add";
  }
  ToggleAccordion2() {
    if (this.AccordionExpanded2) {
      this.renderer.setElementStyle(
        this.cardContent2.nativeElement,
        "max-height",
        "0px"
      );
      this.renderer.setElementStyle(
        this.cardContent2.nativeElement,
        "padding",
        "0px 16px"
      );
    } else {
      this.renderer.setElementStyle(
        this.cardContent2.nativeElement,
        "max-height",
        "500px"
      );
      this.renderer.setElementStyle(
        this.cardContent2.nativeElement,
        "padding",
        "13px 16px"
      );
    }
    this.AccordionExpanded2 = !this.AccordionExpanded2;
    this.icon2 = this.icon2 == "md-add" ? "md-remove" : "md-add";
  }
  ToggleAccordion3() {
    if (this.AccordionExpanded3) {
      this.renderer.setElementStyle(
        this.cardContent3.nativeElement,
        "max-height",
        "0px"
      );
      this.renderer.setElementStyle(
        this.cardContent3.nativeElement,
        "padding",
        "0px 16px"
      );
    } else {
      this.renderer.setElementStyle(
        this.cardContent3.nativeElement,
        "max-height",
        "500px"
      );
      this.renderer.setElementStyle(
        this.cardContent3.nativeElement,
        "padding",
        "13px 16px"
      );
    }
    this.AccordionExpanded3 = !this.AccordionExpanded3;
    this.icon3 = this.icon3 == "md-add" ? "md-remove" : "md-add";
  }
  ToggleAccordion4() {
    if (this.AccordionExpanded4) {
      this.renderer.setElementStyle(
        this.cardContent4.nativeElement,
        "max-height",
        "0px"
      );
      this.renderer.setElementStyle(
        this.cardContent4.nativeElement,
        "padding",
        "0px 16px"
      );
    } else {
      this.renderer.setElementStyle(
        this.cardContent4.nativeElement,
        "max-height",
        "500px"
      );
      this.renderer.setElementStyle(
        this.cardContent4.nativeElement,
        "padding",
        "13px 16px"
      );
    }
    this.AccordionExpanded4 = !this.AccordionExpanded4;
    this.icon4 = this.icon4 == "md-add" ? "md-remove" : "md-add";
  }
}
