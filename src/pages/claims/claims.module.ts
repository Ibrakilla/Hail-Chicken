import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { ClaimsComponent } from './claims';

@NgModule({
  declarations: [
    ClaimsComponent,
  ],
  imports: [
    IonicPageModule.forChild(ClaimsComponent),
    TranslateModule
  ],
  exports: [
    ClaimsComponent,
    TranslateModule
  ]
})
export class ClaimsModule {}
