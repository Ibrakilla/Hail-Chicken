import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { LocationComponent } from './location';


@NgModule({
  declarations: [
    LocationComponent,
  ],
  imports: [
    IonicPageModule.forChild(LocationComponent),
    TranslateModule
  ],
  exports: [
    LocationComponent,
    TranslateModule
  ]
})
export class AboutUsModule {}
